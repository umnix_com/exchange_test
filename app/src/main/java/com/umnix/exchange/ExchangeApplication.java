package com.umnix.exchange;

import android.app.Application;
import android.content.Context;
import android.os.Handler;

import com.birbit.android.jobqueue.JobManager;
import com.umnix.exchange.dagger.ApplicationComponent;
import com.umnix.exchange.dagger.ApplicationModule;
import com.umnix.exchange.dagger.DaggerApplicationComponent;
import com.umnix.exchange.job.LoadRatesJob;
import com.umnix.exchange.model.Currency;
import com.umnix.exchange.model.Wallet;

import javax.inject.Inject;

import timber.log.Timber;

public class ExchangeApplication extends Application {

    private static final Long REQUEST_PERIOD = 30_000L;

    private static ApplicationComponent applicationComponent;

    private static Context context;

    @Inject
    protected JobManager jobManager;

    @Inject
    protected SharedPreferenceManager sharedPreferenceManager;

    @Inject
    protected Wallet wallet;

    private Handler loadRatesHandler;


    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();

        setComponent(DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build());

        getComponent().inject(this);

        initLogging();

        initUserData();
        initRatesLoading();
    }

    public static Context getContext() {
        return context;
    }

    public static ApplicationComponent getComponent() {
        return applicationComponent;
    }

    public void setComponent(ApplicationComponent applicationComponent) {
        this.applicationComponent = applicationComponent;
    }

    private void initLogging() {
        if (!BuildConfig.ENABLE_LOGS) {
            return;
        }

        Timber.plant(new Timber.DebugTree() {
            @Override
            protected String createStackElementTag(StackTraceElement element) {
                return String.format("C:%s:%s",
                        super.createStackElementTag(element),
                        element.getLineNumber());
            }
        });
    }

    private void initUserData() {
        //FIXME : uncomment after swithing to DB storage
        /*if (sharedPreferenceManager.isWalletInitiated()) {
            return;
        }*/

        wallet.setCurrencySum(Currency.USD, Double.valueOf(getString(R.string.USD)));
        wallet.setCurrencySum(Currency.EUR, Double.valueOf(getString(R.string.EUR)));
        wallet.setCurrencySum(Currency.GBP, Double.valueOf(getString(R.string.GBP)));

        //sharedPreferenceManager.setWalletInitiated();
    }

    private void initRatesLoading() {

        loadRatesHandler = new Handler();
        jobManager.start();

        loadRatesHandler.postDelayed(new Runnable() {
            public void run() {
                jobManager.addJobInBackground(new LoadRatesJob());
                loadRatesHandler.postDelayed(this, REQUEST_PERIOD);
            }
        }, 0);
    }
}
