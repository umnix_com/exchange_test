package com.umnix.exchange.model;

import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class CurrencyRates {
    private Map<Currency, DateRate> rates;

    public CurrencyRates() {
        rates = new HashMap<>();
    }

    public String formCurrencyRateString(@NonNull Currency source, @NonNull Currency target) {
        if (source.equals(target)) {
            return null;
        }

        DateRate targetDateRate = getCurrencyRate(target);
        if (targetDateRate == null) {
            return null;
        }

        Double targetRateSum = targetDateRate.getRate(source);
        return String.format(Locale.ENGLISH, "%s 1 = %s %.4f",
                target.getSymbol(), source.getSymbol(), targetRateSum);
    }

    public DateRate getCurrencyRate(@NonNull Currency currency) {
        return rates.containsKey(currency) ?
                rates.get(currency) : null;
    }

    public void setCurrencyRate(@NonNull DateRate rate) {
        rates.put(rate.getCurrency(), rate);
    }
}
