package com.umnix.exchange.model;

public enum Currency {
    EUR("€"),
    USD("$"),
    GBP("£");

    private String symbol;

    public static boolean isSupported(String currencyCode) {
        for (Currency item : Currency.values()) {
            if (item.name().equals(currencyCode)) {
                return true;
            }
        }

        return false;
    }

    Currency(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }
}
