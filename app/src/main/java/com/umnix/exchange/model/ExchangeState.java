package com.umnix.exchange.model;


import android.support.annotation.NonNull;

import com.umnix.exchange.ExchangeApplication;
import com.umnix.exchange.bus.ExchangeStateBus;

import javax.inject.Inject;

public class ExchangeState {

    @Inject
    ExchangeStateBus exchangeStateBus;

    @Inject
    protected CurrencyRates currencyRates;

    private Currency source, target;
    private double sum;

    public ExchangeState() {
        ExchangeApplication.getComponent().inject(this);

        source = Currency.values()[0];
        target = Currency.values()[1];
    }

    public double calcTargetSum() {
        if (sum == 0) {
            return 0;
        }
        DateRate currencyRate = currencyRates.getCurrencyRate(target);
        return currencyRate == null ? 0 : sum * currencyRate.getRate(source);
    }

    public Currency getSource() {
        return source;
    }

    public void setSource(@NonNull Currency source) {
        boolean isChanged = !this.source.equals(source);
        this.source = source;

        onChangedState(isChanged);
    }

    public Currency getTarget() {
        return target;
    }

    public void setTarget(@NonNull Currency target) {
        boolean isChanged = !this.target.equals(target);
        this.target = target;

        onChangedState(isChanged);
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        boolean isChanged = this.sum != sum;

        this.sum = sum;
        onChangedState(isChanged);
    }

    private void onChangedState(boolean isChanged){
        if (isChanged) {
            exchangeStateBus.setExchangeState(this);
        }
    }
}
