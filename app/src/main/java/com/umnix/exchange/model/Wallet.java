package com.umnix.exchange.model;


import android.support.annotation.NonNull;

import com.umnix.exchange.ExchangeApplication;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import timber.log.Timber;

public class Wallet {

    @Inject
    CurrencyRates currencyRates;

    private Map<Currency, Double> money;

    public Wallet() {
        ExchangeApplication.getComponent().inject(this);
        money = new HashMap<>();
    }

    public boolean isValidExchange(@NonNull ExchangeState exchangeState) {
        Currency source = exchangeState.getSource();
        Currency target = exchangeState.getTarget();
        Double sum = exchangeState.getSum();

        if (sum <= 0) {
            return false;
        }

        if (source.equals(target)) {
            return false;
        }

        double walletSum = getCurrencySum(source);
        if (walletSum < sum) {
            return false;
        }

        Double targetSum = exchangeState.calcTargetSum();
        return targetSum > 0;
    }

    public boolean performExchange(@NonNull ExchangeState exchangeState) {
        Currency source = exchangeState.getSource();
        Currency target = exchangeState.getTarget();
        Double sum = exchangeState.getSum();

        Timber.d("exchange operation for %s%.2f to %s", source.getSymbol(), sum, target.name());

        if (!isValidExchange(exchangeState)) {
            Timber.w("Not valid exchange operation for %s%.2f", source.getSymbol(), sum);
            return false;
        }

        setCurrencySum(source, getCurrencySum(source) - sum);
        setCurrencySum(target, getCurrencySum(target) + exchangeState.calcTargetSum());

        return true;
    }

    public double getCurrencySum(@NonNull Currency currency) {
        return money.containsKey(currency) ? money.get(currency) : 0;
    }

    public void setCurrencySum(@NonNull Currency currency, double sum) {
        money.put(currency, sum);
    }
}
