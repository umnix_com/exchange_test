package com.umnix.exchange.model;


import android.support.annotation.NonNull;

import java.util.Date;
import java.util.Map;

public class DateRate {

    private Currency currency;

    private Date date;

    private Map<Currency, Double> currencyRates;

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Map<Currency, Double> getCurrencyRates() {
        return currencyRates;
    }

    public void setCurrencyRates(@NonNull Map<Currency, Double> currencyRates) {
        this.currencyRates = currencyRates;
    }

    public double getRate(@NonNull Currency currency) {
        if (!currencyRates.containsKey(currency)) {
            return 0;
        }

        return currencyRates.get(currency);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DateRate)) return false;

        DateRate dateRate = (DateRate) o;

        if (currency != dateRate.currency) return false;
        if (date != null ? !date.equals(dateRate.date) : dateRate.date != null) return false;

        if (currencyRates == null && dateRate.currencyRates == null) {
            return true;
        }
        if (currencyRates == null || dateRate.currencyRates == null) {
            return false;
        }
        if (currencyRates.size() != dateRate.currencyRates.size()) {
            return false;
        }

        for (Currency rateCurrency : currencyRates.keySet()) {
            Double rateValue = currencyRates.get(rateCurrency);
            if (!rateValue.equals(dateRate.currencyRates.get(rateCurrency))) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = currency != null ? currency.hashCode() : 0;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (currencyRates != null ? currencyRates.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DateRate{" +
                "currency='" + currency + '\'' +
                ", date=" + date +
                ", currencyRates=" + currencyRates +
                '}';
    }
}
