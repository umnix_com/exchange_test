package com.umnix.exchange.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.umnix.exchange.ExchangeApplication;
import com.umnix.exchange.R;
import com.umnix.exchange.model.Currency;
import com.umnix.exchange.model.CurrencyRates;
import com.umnix.exchange.model.ExchangeState;
import com.umnix.exchange.model.Wallet;

import java.lang.ref.WeakReference;
import java.math.BigDecimal;

import javax.inject.Inject;

public class CurrencyAdapter extends PagerAdapter {

    @Inject
    Wallet wallet;

    @Inject
    ExchangeState exchangeState;

    @Inject
    CurrencyRates currencyRates;

    private WeakReference<Context> context;

    public CurrencyAdapter(Context context) {
        ExchangeApplication.getComponent().inject(this);

        this.context = new WeakReference<>(context);
    }

    @Override
    public int getCount() {
        return Currency.values().length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(context.get());

        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.currency_input, container, false);
        container.addView(layout);

        fillValues(container, Currency.values()[position], layout);

        return layout;
    }

    private void fillValues(ViewGroup container, Currency currency, ViewGroup layout) {
        TextView currencyText = layout.findViewById(R.id.currency);
        TextView currencySumText = layout.findViewById(R.id.wallet_amount);
        TextView rateText = layout.findViewById(R.id.current_rate);
        EditText sumText = layout.findViewById(R.id.sum);

        currencyText.setText(currency.name());
        Double sum = wallet.getCurrencySum(currency);
        currencySumText.setText(context.get().getString(R.string.user_amount, currency.getSymbol(), sum));

        //TODO : refactor
        switch ((String) container.getTag()) {
            case "currency_target":
                sumText.setEnabled(false);

                String rate = currencyRates.formCurrencyRateString(
                        exchangeState.getSource(), exchangeState.getTarget());

                sumText.addTextChangedListener(new ExchangeSumWatcher(sumText, false, null));

                if (rate == null) {
                    rateText.setVisibility(View.INVISIBLE);
                } else {
                    rateText.setText(rate);
                    Double targetSum = exchangeState.calcTargetSum();
                    String targetSumString = new BigDecimal(targetSum).toPlainString();
                    sumText.setText(targetSumString);
                }
                break;
            case "currency_source":
                rateText.setVisibility(View.INVISIBLE);

                double sourceSum = exchangeState.getSum();
                sumText.setText(sourceSum == 0 ? "0" : String.valueOf(exchangeState.getSum()));

                sumText.addTextChangedListener(new ExchangeSumWatcher(sumText, true, value -> {
                    exchangeState.setSum(value);
                    checkSumLimit(currency, currencySumText);
                }));
                checkSumLimit(currency, currencySumText);
                break;
        }
    }

    private void checkSumLimit(Currency currency, TextView currencySumText) {
        Double sum = wallet.getCurrencySum(currency);

        if (exchangeState.getSum() > sum) {
            currencySumText.setTextColor(context.get().getResources().getColor(R.color.overLimit));
        } else {
            currencySumText.setTextColor(context.get().getResources().getColor(R.color.currencyText));
        }
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object view) {
        container.removeView((View) view);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }
}
