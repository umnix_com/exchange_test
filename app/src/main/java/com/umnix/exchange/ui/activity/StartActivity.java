package com.umnix.exchange.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.widget.TextView;

import com.umnix.exchange.BuildConfig;
import com.umnix.exchange.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StartActivity extends AppCompatActivity {

    @BindView(R.id.version_text)
    TextView versionTextView;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        ButterKnife.bind(this);

        versionTextView.setText(getString(R.string.version, BuildConfig.VERSION_NAME));
    }


    @OnClick(R.id.exchange_button)
    protected void openExchange() {
        startActivity(new Intent(this, ExchangeActivity.class));
    }
}
