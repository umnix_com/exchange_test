package com.umnix.exchange.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.umnix.exchange.ExchangeApplication;
import com.umnix.exchange.R;
import com.umnix.exchange.bus.RatesBus;
import com.umnix.exchange.model.Currency;
import com.umnix.exchange.model.CurrencyRates;
import com.umnix.exchange.model.DateRate;
import com.umnix.exchange.model.ExchangeState;
import com.umnix.exchange.bus.ExchangeStateBus;
import com.umnix.exchange.model.Wallet;
import com.umnix.exchange.ui.adapter.CurrencyAdapter;

import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;
import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

public class ExchangeActivity extends AppCompatActivity {

    @BindView(R.id.currency_source)
    protected ViewPager sourcePager;
    @BindView(R.id.currency_target)
    protected ViewPager targetPager;
    @BindView(R.id.source_pager_indicator)
    protected CircleIndicator sourcePagerIndicator;
    @BindView(R.id.target_pager_indicator)
    protected CircleIndicator targetPagerIndicator;
    @BindView(R.id.apply_button)
    protected ImageButton applyButton;

    @BindView(R.id.rate_button)
    protected View rateButton;
    @BindView(R.id.rate_source_symbol)
    protected TextView rateSourceSymbol;
    @BindView(R.id.rate_target_symbol)
    protected TextView rateTargetSymbol;
    @BindView(R.id.rate_target_sum_1)
    protected TextView rateTargetSum1;
    @BindView(R.id.rate_target_sum_2)
    protected TextView rateTargetSum2;

    @Inject
    protected RatesBus ratesBus;
    @Inject
    protected ExchangeStateBus exchangeStateBus;
    @Inject
    protected CurrencyRates currencyRates;
    @Inject
    protected ExchangeState exchangeState;
    @Inject
    protected Wallet wallet;

    private CurrencyAdapter sourceAdapter;
    private CurrencyAdapter targetAdapter;

    private CompositeSubscription subscriptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ExchangeApplication.getComponent().inject(this);

        setContentView(R.layout.activity_exchange);

        ButterKnife.bind(this);

        initCurrencySourcePager();
        initCurrencyTargetPager();

        subscriptions = new CompositeSubscription();
        subscribeToExchangeState(subscriptions);
        subscribeToDateRates(subscriptions);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unsubscribe();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showExchangeState(exchangeState);
    }

    private void initCurrencyTargetPager() {
        targetAdapter = new CurrencyAdapter(this);
        targetPager.setAdapter(targetAdapter);
        targetPagerIndicator.setViewPager(targetPager);

        targetPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Currency target = Currency.values()[position];
                exchangeState.setTarget(target);
                targetAdapter.notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void initCurrencySourcePager() {
        sourceAdapter = new CurrencyAdapter(this);
        sourcePager.setAdapter(sourceAdapter);
        sourcePagerIndicator.setViewPager(sourcePager);

        sourcePager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                Currency source = Currency.values()[position];
                exchangeState.setSource(source);
                exchangeState.setSum(0);
                sourceAdapter.notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @OnClick(R.id.exchange_back_button)
    protected void onBackButton() {
        finish();
    }

    @OnClick(R.id.rate_button)
    protected void onRatesButton() {
        startActivity(new Intent(this, RatesActivity.class));
    }

    @OnClick(R.id.apply_button)
    protected void onApplyButton() {
        if (!wallet.performExchange(exchangeState)) {
            //TODO : notify user about not performed operation
            applyButton.setEnabled(false);
            return;
        }

        exchangeState.setSum(0);
        finish();
    }

    private void showExchangeState(@NonNull ExchangeState exchangeState) {
        targetAdapter.notifyDataSetChanged();

        applyButton.setEnabled(wallet.isValidExchange(exchangeState));

        sourcePager.setCurrentItem(exchangeState.getSource().ordinal());
        targetPager.setCurrentItem(exchangeState.getTarget().ordinal());

        showDateRate(exchangeState);
    }

    private void showDateRate(@NonNull ExchangeState exchangeState) {
        Currency source = exchangeState.getSource();
        Currency target = exchangeState.getTarget();

        if (source.equals(target)) {
            rateButton.setVisibility(View.INVISIBLE);
            return;
        }

        DateRate dateRate = currencyRates.getCurrencyRate(source);
        if (dateRate == null) {
            rateButton.setVisibility(View.INVISIBLE);
            return;
        }
        rateButton.setVisibility(View.VISIBLE);

        //TODO : change to Spannable
        rateSourceSymbol.setText(source.getSymbol());
        rateTargetSymbol.setText(target.getSymbol());


        Double rateSum = dateRate.getRate(target);
        rateTargetSum1.setText(String.format(Locale.ENGLISH, "%.2f", rateSum));
        rateTargetSum2.setText(String.format(Locale.ENGLISH, "%.4f", rateSum).substring(4, 6));
    }

    protected void subscribeToExchangeState(CompositeSubscription subscriptions) {
        subscriptions.add(exchangeStateBus.onExchangeStateAvailable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        this::onChangedExchangeState,
                        Timber::e
                )
        );
    }

    protected void subscribeToDateRates(CompositeSubscription subscriptions) {
        subscriptions.add(ratesBus.onDateRatesAvailable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        this::onChangedRates,
                        Timber::e
                )
        );
    }

    private void unsubscribe() {
        if (subscriptions != null) {
            subscriptions.clear();
            subscriptions = null;
        }
    }

    private void onChangedExchangeState(ExchangeState newExchangeState) {
        showExchangeState(newExchangeState);
    }

    private void onChangedRates(DateRate dateRate) {
        showExchangeState(exchangeState);
    }
}
