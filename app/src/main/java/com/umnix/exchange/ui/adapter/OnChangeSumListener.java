package com.umnix.exchange.ui.adapter;

public interface OnChangeSumListener {

    void onChangeSum(double value);
}
