package com.umnix.exchange.ui.activity;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.umnix.exchange.ExchangeApplication;
import com.umnix.exchange.R;

import butterknife.ButterKnife;

public class RatesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ExchangeApplication.getComponent().inject(this);

        setContentView(R.layout.activity_rates);

        ButterKnife.bind(this);
    }
}
