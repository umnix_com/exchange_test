package com.umnix.exchange.ui.adapter;

import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.RelativeSizeSpan;
import android.widget.EditText;

public class ExchangeSumWatcher implements TextWatcher {

    private static String DEFAULT_DECIMAL_SEPARATOR = ".";
    private static String NEGATIVE_SIGN = "-";
    private static String POSITIVE_SIGN = "+";
    private static int MAX_LENGTH = 8;
    private static float DIGITS_SCALE = 0.8f;

    private EditText sumEditText;
    private OnChangeSumListener onChangeSumListener;
    private String sign;
    private boolean isSource;

    private static String getDecimalSeparator() {
        return DEFAULT_DECIMAL_SEPARATOR;
    }

    ExchangeSumWatcher(@NonNull EditText sumEditText, boolean isSource,
                       OnChangeSumListener onChangeSumListener) {
        this.sumEditText = sumEditText;
        this.isSource = isSource;
        this.sign = isSource ? NEGATIVE_SIGN : POSITIVE_SIGN;
        this.onChangeSumListener = onChangeSumListener;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        String sum = editable.toString();

        SumBuilder sumBuilder = new SumBuilder(sum)
                .removeSign(sign)
                .notEmpty();
        sumBuilder = isSource
                ? sumBuilder.limit()
                : sumBuilder.defaultValue();

        sum = sumBuilder
                .separatorPosition()
                .decimalLimit()
                .leadingSign(sign)
                .build();


        SpannableString text = new SpannableString(sum);
        scaleText(text);

        int pos = sumEditText.getSelectionStart();
        if (pos > sum.length()) {
            pos = sum.length();
        }

        sumEditText.removeTextChangedListener(this);
        sumEditText.setText(text);
        sumEditText.setSelection(pos);
        sumEditText.addTextChangedListener(this);

        if (onChangeSumListener != null) {
            onChangeSumListener.onChangeSum(Math.abs(Double.valueOf(text.toString())));
        }
    }

    private void scaleText(SpannableString text) {
        if (text.length() > MAX_LENGTH - 1) {
            text.setSpan(new RelativeSizeSpan(DIGITS_SCALE), 0, text.length(), 0);
        }

        int separatorPos = text.toString().indexOf(getDecimalSeparator());
        if (separatorPos > 0) {
            text.setSpan(new RelativeSizeSpan(DIGITS_SCALE), separatorPos, text.length(), 0);
        }
    }

    private class SumBuilder {

        private String DEFAULT_VALUE = "0";
        private int DECIMAL_MAX_LENGTH = 2;

        private String sum;

        SumBuilder(@NonNull String sum) {
            this.sum = sum;
        }

        SumBuilder removeSign(String sign) {
            sum = sum.replace(sign, "");
            return this;
        }

        SumBuilder notEmpty() {
            if (sum.isEmpty()) {
                sum = DEFAULT_VALUE;
            }

            return this;
        }

        SumBuilder limit() {
            if (sum.length() > MAX_LENGTH) {
                sum = sum.substring(0, MAX_LENGTH);
            }

            return this;
        }

        SumBuilder defaultValue() {
            if (Double.valueOf(sum) == 0) {
                sum = DEFAULT_VALUE;
            }

            return this;
        }

        SumBuilder separatorPosition() {
            int delimiterPos = sum.indexOf(getDecimalSeparator());
            if (delimiterPos == 0) {
                sum = DEFAULT_VALUE + sum;
            }

            return this;
        }

        SumBuilder decimalLimit() {
            int delimiterPos = sum.indexOf(getDecimalSeparator());
            if (delimiterPos > 0 && (sum.length() > delimiterPos + DECIMAL_MAX_LENGTH + 1)) {
                sum = sum.substring(0, delimiterPos + DECIMAL_MAX_LENGTH + 1);
            }

            return this;
        }

        SumBuilder leadingSign(String sign) {
            if (!DEFAULT_VALUE.equals(sum) && !sum.contains(sign)) {
                sum = sign + sum;
            }

            return this;
        }

        String build() {
            return sum;
        }
    }
}
