package com.umnix.exchange.net;

import com.umnix.exchange.model.DateRate;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface RatesApi {
    @GET("latest")
    Observable<Response<DateRate>> getAllLatestRates(@Query("base") String base);

    @GET("latest")
    Observable<Response<DateRate>> getLatestRates(@Query("base") String base, @Query("symbols") String symbols);
}
