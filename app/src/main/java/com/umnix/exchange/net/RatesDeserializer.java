package com.umnix.exchange.net;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.umnix.exchange.model.Currency;
import com.umnix.exchange.model.DateRate;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;


public class RatesDeserializer implements JsonDeserializer<DateRate> {

    private static DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    public static GsonConverterFactory buildGsonConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();

        // Adding custom deserializer
        gsonBuilder.registerTypeAdapter(DateRate.class, new RatesDeserializer());
        Gson gson = gsonBuilder.create();

        return GsonConverterFactory.create(gson);
    }


    @Override
    public DateRate deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        DateRate result = new DateRate();

        String currencyCode = ((JsonObject) json).get("base").getAsString();
        if (!Currency.isSupported(currencyCode)) {
            Timber.e("Requested not supported currency: %s", currencyCode);
            return null;
        }
        result.setCurrency(Currency.valueOf(currencyCode));

        try {
            Date date = df.parse(((JsonObject) json).get("date").getAsString());
            result.setDate(date);
        } catch (ParseException e) {
            Timber.e(e, "Error parsing rate date");
        }

        JsonObject rawRates = ((JsonObject) json).get("rates").getAsJsonObject();
        Map<Currency, Double> currencyRates = new HashMap<>();

        for (Map.Entry<String, JsonElement> elem : rawRates.entrySet()) {
            if (!Currency.isSupported(elem.getKey())) {
                continue;
            }
            double rate = elem.getValue().getAsDouble();
            currencyRates.put(Currency.valueOf(elem.getKey()), rate);
        }

        result.setCurrencyRates(currencyRates);

        return result;
    }
}
