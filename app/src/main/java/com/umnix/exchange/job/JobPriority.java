package com.umnix.exchange.job;

public interface JobPriority {
    int XHIGH = 200;
    int HIGH = 100;
    int NORMAL = 50;
    int LOW = 10;
    int XLOW = 1;
}
