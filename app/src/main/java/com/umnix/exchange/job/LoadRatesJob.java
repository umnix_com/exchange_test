package com.umnix.exchange.job;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.CancelReason;
import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.umnix.exchange.ExchangeApplication;
import com.umnix.exchange.bus.RatesBus;
import com.umnix.exchange.model.Currency;
import com.umnix.exchange.model.CurrencyRates;
import com.umnix.exchange.model.DateRate;
import com.umnix.exchange.net.RatesApi;

import javax.inject.Inject;

import retrofit2.Response;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

public class LoadRatesJob extends Job {

    @Inject
    RatesApi ratesApi;

    @Inject
    RatesBus ratesBus;

    @Inject
    protected CurrencyRates currencyRates;

    private CompositeSubscription subscriptions = new CompositeSubscription();

    public LoadRatesJob() {
        super(new Params(JobPriority.NORMAL)
                .groupBy("load-rates")
                .requireNetwork());
    }

    @Override
    final public void onAdded() {
        ExchangeApplication.getComponent().inject(this);
    }

    @Override
    protected void onCancel(@CancelReason int cancelReason, @Nullable Throwable throwable) {
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        return RetryConstraint.CANCEL;
    }

    @Override
    final public void onRun() throws Throwable {
        ExchangeApplication.getComponent().inject(this);

        for (Currency currency : Currency.values()) {
            subscriptions.add(createDateRatesSubscription(currency));
        }
    }

    private Subscription createDateRatesSubscription(Currency currency) {

        return ratesApi.getAllLatestRates(currency.name())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(dateRatesResponse -> {
                    if (dateRatesResponse.isSuccessful()) {
                        Timber.d("Loaded rates: %s", dateRatesResponse.body());
                        onRatesLoaded(dateRatesResponse.body());
                    } else {
                        Timber.e("Error on loading rates");
                    }
                }, Timber::e);
    }

    private void onRatesLoaded(DateRate dateRate) {
        if (dateRate == null) {
            Timber.w("Rates were not loaded");
            return;
        }

        DateRate currentRate = currencyRates.getCurrencyRate(dateRate.getCurrency());
        if (dateRate.equals(currentRate)) {
            return;
        }

        currencyRates.setCurrencyRate(dateRate);
        ratesBus.setDateRates(dateRate);
    }
}
