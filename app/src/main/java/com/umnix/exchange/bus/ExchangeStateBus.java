package com.umnix.exchange.bus;

import com.umnix.exchange.model.ExchangeState;

import rx.Observable;
import rx.subjects.PublishSubject;

public class ExchangeStateBus {

    private PublishSubject<ExchangeState> exchangeStateSubject = PublishSubject.create();


    public void setExchangeState(ExchangeState exchangeState) {
        exchangeStateSubject.onNext(exchangeState);
    }

    public Observable<ExchangeState> onExchangeStateAvailable() {
        return exchangeStateSubject;
    }
}
