package com.umnix.exchange.bus;

import com.umnix.exchange.model.DateRate;

import rx.Observable;
import rx.subjects.PublishSubject;

public class RatesBus {

    private PublishSubject<DateRate> dateRatesSubject = PublishSubject.create();


    public void setDateRates(DateRate dateRate) {
        dateRatesSubject.onNext(dateRate);
    }

    public Observable<DateRate> onDateRatesAvailable() {
        return dateRatesSubject;
    }
}
