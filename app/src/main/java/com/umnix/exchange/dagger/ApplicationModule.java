package com.umnix.exchange.dagger;

import android.content.Context;

import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.config.Configuration;
import com.umnix.exchange.ExchangeApplication;
import com.umnix.exchange.R;
import com.umnix.exchange.SharedPreferenceManager;
import com.umnix.exchange.net.RatesApi;
import com.umnix.exchange.bus.RatesBus;
import com.umnix.exchange.net.RatesDeserializer;
import com.umnix.exchange.model.CurrencyRates;
import com.umnix.exchange.model.ExchangeState;
import com.umnix.exchange.bus.ExchangeStateBus;
import com.umnix.exchange.model.Wallet;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;

@Module
public class ApplicationModule {

    private final ExchangeApplication exchangeApplication;

    public ApplicationModule(ExchangeApplication exchangeApplication) {
        this.exchangeApplication = exchangeApplication;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return this.exchangeApplication;
    }

    @Provides
    @Singleton
    RatesApi provideRatesApiService() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(exchangeApplication.getResources().getString(R.string.rates_url))
                .addConverterFactory(RatesDeserializer.buildGsonConverter())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(client)
                .build();
        return retrofit.create(RatesApi.class);
    }

    @Provides
    @Singleton
    RatesBus provideRatesBus() {
        return new RatesBus();
    }

    @Provides
    @Singleton
    JobManager provideJobManager() {
        Configuration.Builder builder = new Configuration.Builder(exchangeApplication);

        return new JobManager(builder.build());
    }

    @Provides
    @Singleton
    Wallet provideWallet() {
        return new Wallet();
    }

    @Provides
    @Singleton
    SharedPreferenceManager provideSharedPreferenceManager() {
        return new SharedPreferenceManager(exchangeApplication);
    }

    @Provides
    @Singleton
    CurrencyRates provideCurrencyRates() {
        return new CurrencyRates();
    }

    @Provides
    @Singleton
    ExchangeState provideExchangeState() {
        return new ExchangeState();
    }

    @Provides
    @Singleton
    ExchangeStateBus provideExchangeStateBus() {
        return new ExchangeStateBus();
    }
}
