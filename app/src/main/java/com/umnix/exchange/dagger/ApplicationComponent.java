package com.umnix.exchange.dagger;

import com.umnix.exchange.ExchangeApplication;
import com.umnix.exchange.job.LoadRatesJob;
import com.umnix.exchange.model.CurrencyRates;
import com.umnix.exchange.model.ExchangeState;
import com.umnix.exchange.bus.ExchangeStateBus;
import com.umnix.exchange.model.Wallet;
import com.umnix.exchange.ui.adapter.CurrencyAdapter;
import com.umnix.exchange.ui.activity.ExchangeActivity;
import com.umnix.exchange.ui.activity.RatesActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(ExchangeApplication application);

    void inject(ExchangeActivity exchangeActivity);

    void inject(LoadRatesJob job);

    void inject(CurrencyAdapter adapter);

    void inject(Wallet wallet);

    void inject(RatesActivity ratesActivity);

    void inject(CurrencyRates currencyRates);

    void inject(ExchangeState exchangeState);

    void inject(ExchangeStateBus exchangeStateBus);
}