package com.umnix.exchange;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceManager {

    private static final String SHARED_PREFERENCE_KEY = "exchange";
    private static final String IS_WALLET_INITIATED = "is.wallet.initiated";

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public SharedPreferenceManager(Context context) {
        sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCE_KEY, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }


    public Boolean isWalletInitiated() {
        return sharedPreferences.getBoolean(IS_WALLET_INITIATED, false);
    }

    public void setWalletInitiated() {
        editor.putBoolean(IS_WALLET_INITIATED, true).apply();
    }
}
